const express = require('express');
const path = require('path');

const app = express();

app.use(express.static('./dist/angular-peliculas-master'));

app.get('/*', (req, res) =>
    res.sendFile('index.html', {root: 'dist/angularpeliculasmaster/'}),
);

app.listen(process.env.PORT || 8080);
